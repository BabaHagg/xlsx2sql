##
## XLSX FILE TO SQL SCRIPT
##    DEV BY BABAHAGG
##          v2
##

import xlrd
import readline
import json
import sys

f = open('conf.json')
data = json.load(f)

readline.parse_and_bind("tab: complete")
configPerso = ""
volcab = ['char', 'varchar()', 'int', 'bool', 'timestamp', 'text', 'json', 'array']

def complete(text,state):
    results = [x for x in volcab if x.startswith(text)] + [None]
    return results[state]

class Formatter:
  def __init__(self, path):
    self.path = path
    self.book = xlrd.open_workbook(path)
    self.workbook=""
    self.worksheet=""
    self.sheet_name=""
    self.table_name=""
    self.num_rows=1
    self.num_cells=1
    self.curr_row=1
    self.columnsDetails=[]
    self.edit="false"
    self.col = []
    self.colType = []
    self.constr = []

  def setBook(self):
    self.book = xlrd.open_workbook(self.path)

  def setWorksheet(self, sheet_name, edit):
    self.sheet_name = sheet_name
    if edit == "false":
      ques = input('Même nom pour le sheet_name et la table SQL ? y/n \n')
      self.table_name = self.sheet_name if ques == 'y' else input('Nom de la table : \n')
    self.worksheet = self.book.sheet_by_name(self.sheet_name)
    self.num_rows = self.worksheet.nrows - 1
    self.num_cells = self.worksheet.ncols - 1
    self.curr_row = -1

  def printSheetsNames(self):
    print("Liste des noms du sheet : \n")
    arr = self.book.sheet_names()
    for i in range(len(arr)):
      print(arr[i])
    print("\n")

  def f(self, x):
    return {
        0:'empty',
        1:'CHAR',
        2:'INT',
        3:'DATETIME',
        4:'BOOLEAN',
        5:'error',
        6:'null',
    }[x]

  def writeFile(self):
    query = self.generate(self.num_rows, self.num_cells, self.curr_row)
    file = open("./sql/" + self.sheet_name + ".txt", "a")
    file.write(query)
    file.close()

  def setColOptions(self, value):
    nullable = input(f"{value} est nullable ? : y/n\n")
    null = "" if nullable == "y" else " NOT NULL"
    print('\n')
    constr = input("Ajouter une constraintes ? : y/n\n")
    null += "" if constr == 'n' else " " + input('Quel est la constraintes : \n')
    return null

  def setConstraints(self):
    constr = ""
    if self.edit == "true":
      for x in self.constr:
        #end = ");" if x == len(self.col) -1 else ", "
        constr += x + "\n" if x else ""
      return constr
    return


  def setColumns(self):
    cols = ""
    if self.edit == "true":
      for x in range(len(self.col)):
        end = ");\n" if x == len(self.col) -1 else ", "
        cols += self.col[x] + " " + self.colType[x] + end
      return cols
    for x in range(self.num_cells + 1):
      value = self.worksheet.cell_value(0, x)
      print("Nom : " + value + "\n")
      readline.set_completer(complete)
      print("[ char, varchar(50), int, bool, timestamp, text, json, array ]\n")
      type = input("Choix du type : \n")
      print(f"le type de {value} est {type}\n")
      self.columnsDetails.append([value, type])
      end = ");" if x == self.num_cells else ", "
      if value == "id":
        cols += value + " INT PRIMARY KEY NOT NULL AUTO_INCREMENT" + end
      else:
        cols += value + " " + type + self.setColOptions(value) + end
      pass
    #print('\n' + cols)
    return cols

  def setInsertInto(self):
    column = ""
    if self.edit == "true":
      for x in range(len(self.col)):
        space = ') VALUES (' if (x == len(self.col) -1) else ', '
        column += self.col[x] + space
      return column
    for x in range(self.num_cells + 1):
      rom_value = self.worksheet.cell_value(0, x)
      space = ') VALUES (' if (x == self.num_cells) else ', '
      rom_value += space
      column += rom_value 
      pass
    return column

  def autoGeneration(self, i):
    sheetname = i['sheetname']
    tablename = i['tablename']
    col = i['col']
    colType = i['type']
    constraints = i['constraints']
    self.col = col
    self.colType = colType
    self.constr = constraints
    self.edit = "true"
    #print(sheetname)
    #print(tablename)
    #print(col)
    #print(colType)
    #for v in range(len(col)):
      #  print(col[v] + " " + colType[v])
    l = self.book.sheet_names()
    for i in l:
      if(sheetname == i):
        worksheet = i
        self.table_name = tablename
        self.setWorksheet(worksheet, "true")
        self.writeFile()
    return

  def generate(self, num_rows, num_cells, curr_row):
    query = ""
    print(f"-- Table {self.table_name} \n")
    query = 'CREATE TABLE IF NOT EXISTS '+ self.table_name + ' (' + self.setColumns()
    curr_row += 1
    #print(num_rows, num_cells, curr_row)
    #print(self.columnsDetails)
    if self.edit == "true":
      query += '\n'
      query += self.setConstraints()
    while curr_row < num_rows:
      query += '\n'
      curr_row += 1
      row = self.worksheet.row(curr_row)
      query += 'INSERT INTO ' + self.table_name + ' ('
      query += self.setInsertInto()
      curr_cell = -1
      rows = ""
      while curr_cell < num_cells:
        curr_cell += 1
        #print(self.columnsDetails[curr_cell])
        cell_type = self.worksheet.cell_type(curr_row, curr_cell)
        cell_value = self.worksheet.cell_value(curr_row, curr_cell)
        #print(self.f(cell_type))
        #typeIs = '\""' if self.f(cell_type) == 'empty' else str(cell_value)
        typeIs = "NULL" if self.f(cell_type) == 'empty' else "'" + str(cell_value) + "'" if self.f(cell_type) == 'CHAR' else str(cell_value)[0:-2] if self.f(cell_type) == 'INT' else str(cell_value)
        typeIs += ");\n" if curr_cell == num_cells else ", "
        rows += typeIs
        query += typeIs
    print(query)
    return query


def main() -> int:
  path = input("Nom et chemin du fichier XLSX : \n")
  x = Formatter(path)
  x.printSheetsNames()
  worksheet = input("Choix de la colonne : \n Nom [Table seul] \n None [Toutes] \n Auto [conf.json] \n Debug [show original col name] \n")
  print("\n")
  if worksheet == 'None':
    l = x.book.sheet_names()
    for i in l:
      if i == 'None' or i == 'Worksheet':
        print("default sheet bypassed .")
      else:
        print(i + "\n")
        worksheet = i
        x.setWorksheet(worksheet, "false")
        x.writeFile()
      return 0
  elif worksheet == 'Auto':
    for i in data['conf']:
      x.autoGeneration(i)
    return 0
  elif worksheet == 'Debug':
    worksheet = input("Choix de la colonne à debug :  \n\n")
    x.sheet_name = worksheet
    x.table_name = worksheet
    x.worksheet = x.book.sheet_by_name(worksheet)
    x.num_rows = x.worksheet.nrows - 1
    x.num_cells = x.worksheet.ncols - 1
    x.curr_row = -1
    curr_cell = -1
    while curr_cell < x.num_cells:
        curr_cell += 1
        cell_type = x.worksheet.cell_type(0, curr_cell)
        cell_value = x.worksheet.cell_value(0, curr_cell)
        print(str(cell_type) + " " + cell_value)
    print("\n")
    f.close()
    return 0
  else:
    x.setWorksheet(worksheet, "false")
    x.writeFile()
    return 0
  return 1

if __name__ == '__main__':
  sys.exit(main())