# XLSX2SQL

* [![Python-xlrd][python.io]][python-url]

## Versions

- Python 3.9.2
- PIP 22.2.2

## Requierements :

```sh
pip install xlrd

```


## Informations :  

- go trough xlsx
- print xlsx table
- convert xlsx table to sql script 
- can edit cols manualy by table or going trough all tables
- can auto edit all tables in xlsx if conf.json is set

## RUN COMMANDS : 

```sh
python xlsx.py

```
```sh
python xlsx.py -h
```

#### more options : 

- *NAME* = Set one table & convert it
- None = Set all table & convert it
- Auto = Set all table find in conf.json & convert it
- Debug = print xlsx table, col, type

## Author BabaHagg

# XLSX2SQL TODO
- [x] Manual convert one
- [x] Auto convert ( manual columns edit )
- [x] Auto convert ( Auto col, json conf file ) 
- [ ] DBInjector Inject all scripts  

[python.io]: https://img.shields.io/pypi/pyversions/xlrd
[python-url]: https://pypi.org/project/xlrd/